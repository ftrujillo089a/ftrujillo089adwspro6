<?php

/* 
 Constantes para pie de página y datos BBDD
 */
  class Config {
    static public $titulo='PROVINCIAS ESPAÑOLAS Y SUS POBLACIONES';
    static public $autor='Paqui Trujillo';
    static public $fecha='2017-01-12';
    static public $empresa='CEEDcv';
    static public $curso='2016-2017';
    static public $tema = "Tema 6. Mysql";
    static public $bdhostname = "localhost";
    static public $bdnombre = "ceedcv";
    static public $bdusuario = "alumno";
    static public $bdclave = "alumno";
    static public $modelo = "mysql";  // para trabajar con ficheros cambiar modelo='fichero'
                                     // con base datos , modelo='mysql'
}
?>
