<?php

/* 
 clase Provincia
 */
class Provincia {
   // Atributos
    private $codigo;
    private $nominacion;
    private $superficie;
    private $habitantes;
    private $comunidad;
    
// constructor    
    public function __construct($codigo,$nominacion,$superficie,$habitantes,$comunidad) {
        $this -> codigo = $codigo;
        $this -> nominacion = $nominacion;
        $this -> superficie = $superficie;
        $this -> habitantes = $habitantes;
        $this -> comunidad = $comunidad;        
    } 
    // Métodos get 
    function getCodigo() {
        return $this->codigo;
    }

    function getNominacion() {
        return $this->nominacion;
    }

    function getSuperficie() {
        return $this->superficie;
    }

    function getHabitantes() {
        return $this->habitantes;
    }

    function getComunidad() {
        return $this->comunidad;
    }
// Métodos set
    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    function setNominacion($nominacion) {
        $this->nominacion = $nominacion;
    }

    function setSuperficie($superficie) {
        $this->superficie = $superficie;
    }

    function setHabitantes($habitantes) {
        $this->habitantes = $habitantes;
    }

    function setComunidad($comunidad) {
        $this->comunidad = $comunidad;
    }


}
?>